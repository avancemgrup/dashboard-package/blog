## Avannubo package blog

Blog

## Install

* Config composer.json: 
    
    require: {
    
        ...
        
        "avannubo/blog": "dev-master"
    }
   
    "repositories": [
        
        ...
        
        {
            
                "type": "vcs",
                
                "url": "https://gitlab.com/avancemgrup/dashboard-package/blog.git"
                
        }
            
    ]

* Install: `composer update`
* Add to Service Provider: `Avannubo\Blog\BlogServiceProvider::class`
* seeders: `php artisan db:seed --class=Avannubo\blog\Seeds\BlogCommentsSettingsSeeder`
* seeders: `php artisan db:seed --class=Avannubo\blog\Seeds\PermissionBlogSeeder`
