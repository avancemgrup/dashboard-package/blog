<?php

namespace Avannubo\Blog;

use Illuminate\Support\ServiceProvider;

class BlogServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {

        //routes
        include __DIR__.'/routes/routes.php';
        //models
        include __DIR__.'/models/BlogPost.php';
        include __DIR__.'/models/BlogCategory.php';
        include __DIR__.'/models/BlogTag.php';
        include __DIR__.'/models/BlogComment.php';
        include __DIR__.'/models/BlogCommentSettings.php';
        //view blade
        $this->loadViewsFrom(__DIR__.'/views', 'blog');
        //migration
        $this->loadMigrationsFrom(__DIR__.'/migrations');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        //register controller
        $this->app->make('Avannubo\Blog\Controllers\BlogCategoryController');
        $this->app->make('Avannubo\Blog\Controllers\BlogPostController');
        $this->app->make('Avannubo\Blog\Controllers\BlogTagController');
        $this->app->make('Avannubo\Blog\Controllers\BlogCommentController');

    }
}
