@extends('layouts.administration.master')

@section('site-title')
    Blog Post
@endsection

@section('main-content')
    <div class="container-fluid">
        <div class="row card">
            <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12">
                <div class="row card__container">
                    <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12">
                        @permission('blog.posts.create')
                        <a href="{{ route('blog-post-add') }}" class="btn btn-success">
                           Nuevo
                        </a>
                        @endpermission
                    </div>
                    <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12">
                        @permission('blog.posts.delete')
                        <button id="removeBtn" class="btn btn-danger">
                            Eliminar los seleccionados
                        </button>
                        @endpermission
                    </div>

                    <div class="col-md-offset-4 col-lg-offset-4 col-md-4 col-lg-4 col-xs-12 col-sm-12">
                        <div class="row end-md end-lg ">
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                 {!! Form::open(array('route' => ['blog-post'], 'method'=>'get')) !!}
                                     {!! Form::text('search', null, array('placeholder' => 'Buscar','class' => 'form-control', 'id' => 'search')) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row card">
            <div class="col-xs-12 col-md-12">
                <h3 class="table__name">Posts</h3>
                @if (Session::has('error'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger">
                                {{ Session::get('error')  }}
                            </div>
                        </div>
                    </div>
                @endif
                @if (Session::has('message'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success">
                                {{ Session::get('message')  }}
                            </div>
                        </div>
                    </div>
                @endif
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="all"></label>
                                </div>
                            </th>
                            <th>Título</th>
                            <th>Descripción</th>
                            <th>Estado</th>
                            <th>Categorias</th>
                            <th>Fecha de publicación</th>
                            @if(Entrust::can('blog.posts.edit') || Entrust::can('blog.posts.delete'))
                            <th>Opciones</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($blogPosts as $blogPost)
                            <tr>
                                <td>
                                    <div>
                                        <input type="checkbox" name="post" data-id="{{ $blogPost->id }}">
                                    </div>
                                </td>
                                <td>{{ $blogPost->title }}</td>
                                <td class="table-ellipsis">{!! $blogPost->description !!}</td>
                                <td>{{ $blogPost->status == 1 ? 'Habilitado' : 'Deshabilitado' }}</td>
                                <td>
                                    @foreach($blogPost->categories as $category)
                                        {{ $category->name }}
                                    @endforeach
                                </td>
                                <td>{{ $blogPost->publication_at }}</td>
                                <td>
                                    @permission('blog.posts.edit')
                                    <a href="{{ route('blog-post-edit', $blogPost->id) }}" class="btn btn-default btn-icon">
                                        <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                                    </a>
                                    @endpermission
                                    @permission('blog.posts.delete')
                                    {!! Form::open(array('route' => ['blog-post-delete', $blogPost->id], 'method'=>'DELETE', 'enctype' => 'multipart/form-data', 'style' => 'display:inline-block')) !!}
                                        <button class="btn btn-danger btn-icon">
                                            <i class="fa fa-trash fa-2x" aria-hidden="true"></i>
                                        </button>
                                    {!! Form::close() !!}
                                    @endpermission
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <hr>
                </div>
                <div class="row middle-xs end-md end-lg">
                    <div class=" col-md-offset-9 col-lg-offset-4 col-md-1 col-xs-3 col-sm-3">
                        <p>{{ $blogPosts->firstItem() }} - {{ $blogPosts->lastItem() }} de {{ $blogPosts->count() }}</p>
                    </div>
                    {{ $blogPosts->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')
@endsection

@section('scripts')
    <script>
        function deletePost() {
            let checkbox = document.querySelectorAll('input[name=post]:checked');
            let posts_id = [];
            for (let i = 0; i < checkbox.length; i++) {
                posts_id.push(checkbox[i].getAttribute("data-id"));
            }

            $.ajax({
                url: "{{ route('blog-posts-delete') }}",
                type: 'DELETE',
                data: {
                    '_token': "{{ csrf_token() }}",
                    'posts_id': posts_id
                },
                complete: function () {
                    window.location.reload();
                }
            })
        }

        function selectAll() {
            let check = document.querySelectorAll("input[name=all]:checked").length;
            let checkbox = document.querySelectorAll('input[name=post]');

            for (let i = 0; i < checkbox.length; i++) {
                checkbox[i].checked = check == 1 ? true : false;
            }

        }
        document.getElementById("removeBtn").addEventListener("click", deletePost);
        document.querySelectorAll("input[name=all]")[0].addEventListener("change", selectAll);
    </script>
@endsection