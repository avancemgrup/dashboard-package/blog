@extends('layouts.administration.master')

@section('site-title')
    Edit Blog Post
@endsection

@section('main-content')
    <div class="container-fluid">
        <div class="row card">
            <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 flex align-bottom">
                <div>
                    <h2>Editar Blog Post {{ $blogPost->id }}</h2>
                </div>
                <div class="no-margin-left">
                    <a class="btn btn-primary" href="{{ route('blog-post') }}">
                       Volver
                    </a>
                </div>
            </div>
        </div>

        <div class="row card">
            <div class="col-xs-12 col-md-12">
                @if(@session('message'))
                    <div class="alert alert-success">
                        {{ @session('message') }}
                    </div>
                @endif
                @if(@session('error'))
                    <div class="alert alert-danger">
                        {{ @session('error') }}
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> Hubo algunos problemas con tu entrada.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::model($blogPost, ['method' => 'POST','route' => ['blog-post-edit', $blogPost->id], 'enctype' => 'multipart/form-data']) !!}
                <input type="hidden" name="_method" value="PUT">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <label>Titulo</label>
                            {!! Form::text('title', null, array('placeholder' => 'Titulo','class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <label>Alias</label>
                            {!! Form::text('slug', null, array('placeholder' => 'Slug','class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <label>Imagen</label>
                            {!! Form::file('image', array('accept' => 'image/*','class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <label>Descripción</label>
                            {!! Form::textarea('description', null, array('placeholder' => 'Descripción','class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <label>Contenido</label>
                            {!! Form::textarea('content', null, array('placeholder' => 'Contenido','class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <label>Estado</label>
                            {!! Form::select('status', ['false' => 'Deshabilitado', 'true' => 'Habilitado'], ($blogPost->status == 1 ? 'true' : 'false'), ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <label>Fecha</label>
                            {{ \Carbon\Carbon::setToStringFormat('Y/m/d') }}
                            {!! Form::date('publicationdate', new \Carbon\Carbon($blogPost->publication_at), ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <label>Hora</label>
                            {{ \Carbon\Carbon::setToStringFormat('H:i') }}
                            {!! Form::time('publicationtime', new \Carbon\Carbon($blogPost->publication_at), ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    @if($categories)
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <label>Categoria</label>
                            @foreach($categories as $category)
                                <strong>{{ Form::checkbox('categories[]', $category->id, in_array($category->id, $categoriesInUser) ? true : false, array('class' => 'name')) }}
                                    {{ $category->name }}</strong>
                            @endforeach
                        </div>
                    </div>
                    @endif
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <label>Etiquetas</label>
                            <div id="tags">
                                @foreach($blogPost->tags as $tag)
                                    <input type="text" name="tags[]" readonly="readonly" value="{{ $tag->name }}" size="{{ strlen($tag->name) }}" class="btn btn-info">
                                @endforeach
                            </div>
                            <br />
                            <input type="text" name="uploadTags" autocomplete="off">
                            <input type="button" name="uploadTag" value="Agregar" class="btn btn-default">
                            <input type="button" name="cleanUploadTags" value="Limpiar" class="btn btn-warning">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 text-left">
                        <button type="submit" class="btn btn-success">
                           Actualizar
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('styles')
@endsection

@section('scripts')
    <script src="{{ asset("js/tinymce/tinymce.min.js") }}"></script>
    <script src="{{ asset("js/tinymce/tinymce_editor.js") }}"></script>
    <script>
        editor_config.path_absolute = "{{ url('/') }}";
        editor_config.selector = "textarea[name=content]";
        tinymce.init(editor_config);

        function string_to_slug(e) {
            var str = e.target.value;
            str = str.replace(/^\s+|\s+$/g, ''); // trim
            str = str.toLowerCase();

            // remove accents, swap ñ for n, etc
            var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
            var to   = "aaaaeeeeiiiioooouuuunc------";
            for (var i=0, l=from.length ; i<l ; i++) {
                str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
            }

            str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                .replace(/\s+/g, '-') // collapse whitespace and replace by -
                .replace(/-+/g, '-'); // collapse dashes

            document.querySelector("input[name=slug]").value = str;
            return false;

        }

        function read_URL(e) {
            var input = e.target;
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    document.getElementById('uploadImage').src = e.target.result;
                };
                reader.readAsDataURL(input.files[0]);
            }
            document.getElementById('uploadImage').style.display = 'inline-block';
        }

        var tags = [];
        var childens = document.getElementById('tags').children;

        for (var i = 0; i < childens.length; i++) {
            tags.push(childens[i].defaultValue);
        }

        function add_tag_input_enter(e) {
            if (e.keyCode == 13) {
                e.preventDefault();

                if (e.target.value.trim() != "") {
                    add_tag(e.target.value);
                }

                e.target.value = "";
            }
            return false;
        }

        function add_tag_input_button() {
            add_tag(document.querySelector("input[name=uploadTags]").value);
            document.querySelector("input[name=uploadTags]").value = "";
        }

        function add_tag(name) {
            var exist = tags.find(function (el) {
                return el == name.trim();
            });
            if (exist == undefined || exist == null) {
                var input = document.createElement('INPUT');
                var space = document.createTextNode("\u00A0");

                input.setAttribute("type", "text");
                input.setAttribute("name", "tags[]");
                input.setAttribute("readonly", "readonly");
                input.setAttribute("value", name.trim());
                input.setAttribute("size", input.value.length);
                input.className = "btn btn-info";

                tags.push(name.trim());
                document.getElementById('tags').appendChild(input);
                document.getElementById('tags').appendChild(space);
            }
        }

        function delete_tag_input(e) {
            tags = [];
            document.getElementById('tags').innerHTML = "";
        }

        document.querySelector("input[name=title]").addEventListener('input',string_to_slug);
        document.querySelector("input[name=image][type=file]").addEventListener('change',read_URL);
        document.querySelector("input[name=uploadTags]").addEventListener('keypress',add_tag_input_enter);
        document.querySelector("input[name=uploadTag]").addEventListener('click',add_tag_input_button);
        document.querySelector("input[name=cleanUploadTags]").addEventListener('click',delete_tag_input);
    </script>
@endsection
