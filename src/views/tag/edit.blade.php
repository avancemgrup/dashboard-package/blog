@extends('layouts.administration.master')

@section('site-title')
    Blog Tag
@endsection

@section('main-content')
    <div class="container-fluid">
        <div class="row card">
            <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 flex align-bottom">
                <div>
                    <h2>Editar Blog Etiqueta {{ $blogTag->id }}</h2>
                </div>
                <div class="no-margin-left">
                    <a class="btn btn-primary" href="{{ route('blog-tag') }}">
                       Volver
                    </a>
                </div>
            </div>
        </div>

        <div class="row card">
            <div class="col-xs-12 col-md-12">
                @if(@session('message'))
                    <div class="alert alert-success">
                        {{ @session('message') }}
                    </div>
                @endif
                @if(@session('error'))
                    <div class="alert alert-danger">
                        {{ @session('error') }}
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> Hubo algunos problemas con tu entrada.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::model($blogTag, ['method' => 'POST','route' => ['blog-tag-edit', $blogTag->id]]) !!}
                <input type="hidden" name="_method" value="PUT">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <label>Nombre</label>
                            {!! Form::text('name', null, array('placeholder' => 'Nombre','class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 text-left">
                        <button type="submit" class="btn btn-success">
                            Actualizar
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('styles')
@endsection

@section('scripts')
@endsection
