@extends('layouts.administration.master')

@section('site-title')
    Blog Category
@endsection

@section('main-content')
    <div class="container-fluid">
        <div class="row card">
            <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 flex align-bottom">
                <div>
                    <h2>Editar Categoria {{ $blogCategory->id }}</h2>
                </div>
                <div class="no-margin-left">
                    <a class="btn btn-primary" href="{{ route('blog-category') }}">
                       Volver
                    </a>
                </div>
            </div>
        </div>

        <div class="row card">
            <div class="col-xs-12 col-md-12">
                @if(@session('message'))
                    <div class="alert alert-success">
                        {{ @session('message') }}
                    </div>
                @endif
                @if(@session('error'))
                    <div class="alert alert-danger">
                        {{ @session('error') }}
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> Hubo algunos problemas con tu entrada.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::model($blogCategory, ['method' => 'POST','route' => ['blog-category-edit', $blogCategory->id]]) !!}
                <input type="hidden" name="_method" value="PUT">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Nombre</strong>
                            {!! Form::text('name', null, array('placeholder' => 'Nombre','class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Slug</strong>
                            {!! Form::text('slug', null, array('placeholder' => 'Descripción','class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Descripción</strong>
                            {!! Form::textarea('description', null, array('placeholder' => 'Descripción','class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 text-left">
                        <button type="submit" class="btn btn-success">
                            Actualizar
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('styles')
@endsection

@section('scripts')
    <script>
        function string_to_slug(e) {
            var str = e.target.value;
            str = str.replace(/^\s+|\s+$/g, ''); // trim
            str = str.toLowerCase();

            // remove accents, swap ñ for n, etc
            var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
            var to   = "aaaaeeeeiiiioooouuuunc------";
            for (var i=0, l=from.length ; i<l ; i++) {
                str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
            }

            str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                .replace(/\s+/g, '-') // collapse whitespace and replace by -
                .replace(/-+/g, '-'); // collapse dashes

            document.querySelector("input[name=slug]").value = str;
            return false;

        }

        document.querySelector("input[name=name]").addEventListener('input',string_to_slug);
    </script>
@endsection