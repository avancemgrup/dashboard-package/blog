@extends('layouts.administration.master')

@section('site-title')
    Blog Comment
@endsection

@section('main-content')
    <div class="container-fluid">
        <div class="row card">
            <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12">
                <div class="row card__container">
                    <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12">
                        @permission('blog.comments.create')
                        <a href="{{ route('blog-comment-add') }}" class="btn btn-success">
                          Nuevo
                        </a>
                        @endpermission
                    </div>
                    <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12">
                        @permission('blog.comments.delete')
                        <button id="removeBtn" class="btn btn-danger">
                           Eliminar los seleccionados
                        </button>
                        @endpermission
                    </div>

                    <div class="col-md-offset-4 col-lg-offset-4 col-md-4 col-lg-4 col-xs-12 col-sm-12">
                        @permission('blog.admin')
                        <a href="{{ route('blog-comment-settings') }}" class="btn btn-default btn-primary pull-right">Configuracion</a>
                        @endpermission
                        <div class="row end-md end-lg ">
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                               {!! Form::open(array('route' => ['blog-comment'], 'method'=>'get')) !!}
                                     {!! Form::text('search', null, array('placeholder' => 'Buscar','class' => 'form-control', 'id' => 'search')) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row card">
            <div class="col-xs-12 col-md-12">
                <h3 class="table__name">Posts</h3>
                @if (Session::has('error'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger">
                                {{ Session::get('error')  }}
                            </div>
                        </div>
                    </div>
                @endif
                @if (Session::has('message'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success">
                                {{ Session::get('message')  }}
                            </div>
                        </div>
                    </div>
                @endif
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="all"></label>
                                </div>
                            </th>
                            <th>Nombre</th>
                            <th>Message</th>
                            <th>Post</th>
                            @if(Entrust::can('blog.comments.create') || Entrust::can('blog.comments.delete'))
                            <th>Opciones</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($blogComments as $blogComment)
                                <tr>
                                    <td>
                                        <div>
                                            <input type="checkbox" name="comments" data-id="{{ $blogComment->id }}">
                                        </div>
                                    </td>
                                    <td>{{ $blogComment->author }}</td>
                                    <td class="table-ellipsis">{{ $blogComment->message }}</td>
                                    <td><a class="btn btn-info" href="{{ route('blog-post-edit', ['id' => $blogComment->post->id]) }}">{{ $blogComment->post->title }}</a></td>
                                    <td>
                                        @permission('blog.comments.edit')
                                        <a href="{{ route('blog-comment-edit', $blogComment->id) }}" class="btn btn-default btn-icon">
                                            <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                                        </a>
                                        @endpermission
                                        @permission('blog.comments.delete')
                                        {!! Form::open(array('route' => ['blog-comment-delete', $blogComment->id], 'method'=>'DELETE', 'enctype' => 'multipart/form-data', 'style' => 'display:inline-block')) !!}
                                        <button class="btn btn-danger btn-icon">
                                            <i class="fa fa-trash fa-2x" aria-hidden="true"></i>
                                        </button>
                                        {!! Form::close() !!}
                                        @endpermission
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="row middle-xs end-md end-lg">
                    {{ $blogComments->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')
@endsection

@section('scripts')
    <script>
        function deleteComment() {
            let checkbox = document.querySelectorAll('input[name=comments]:checked');
            let comments_id = [];
            for (let i = 0; i < checkbox.length; i++) {
                comments_id.push(checkbox[i].getAttribute("data-id"));
            }

            $.ajax({
                url: "{{ route('blog-comments-delete') }}",
                type: 'DELETE',
                data: {
                    '_token': "{{ csrf_token() }}",
                    'comments_id': comments_id
                },
                complete: function () {
                    window.location.reload();
                }
            })
        }

        function selectAll() {
            let check = document.querySelectorAll("input[name=all]:checked").length;
            let checkbox = document.querySelectorAll('input[name=comments]');

            for (let i = 0; i < checkbox.length; i++) {
                checkbox[i].checked = check == 1 ? true : false;
            }

        }
        document.getElementById("removeBtn").addEventListener("click", deleteComment);
        document.querySelectorAll("input[name=all]")[0].addEventListener("change", selectAll);
    </script>
@endsection