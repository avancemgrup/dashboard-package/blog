@extends('layouts.administration.master')

@section('site-title')
    Blog Comment
@endsection

@section('main-content')
    <div class="container-fluid">
        <div class="row card">
            <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 flex align-bottom">
                <div>
                    <h2>Configuración de Comentarios para Blog</h2>
                </div>
                <div class="no-margin-left">
                    <a class="btn btn-primary" href="{{ route('blog-comment') }}">
                        Volver
                    </a>
                </div>
            </div>
        </div>
        <div class="row card">
            <div class="col-xs-12 col-md-12">
                <h3 class="table__name">Configución</h3>
                @if(@session('message'))
                    <div class="alert alert-success">
                        {{ @session('message') }}
                    </div>
                @endif
                @if(@session('error'))
                    <div class="alert alert-danger">
                        {{ @session('error') }}
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> Hubo algunos problemas con tu entrada.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::open(array('route' => 'blog-comment-settings', 'method'=>'PUT')) !!}
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <label for="anonym">Aceptar comentarios anónimos</label>
                            <select name="anonym" id="anonym" class="form-control">
                                <option value="false" {{ $anonym->value == 'false' ? 'selected' : '' }}>No</option>
                                <option value="true" {{ $anonym->value == 'true' ? 'selected' : '' }}>Sí</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 text-left">
                        <button type="submit" class="btn btn-success">
                            Crear
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('styles')
@endsection

@section('scripts')
@endsection