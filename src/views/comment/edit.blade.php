@extends('layouts.administration.master')

@section('site-title')
    Blog Comment
@endsection

@section('main-content')
    <div class="container-fluid">
        <div class="row card">
            <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 flex align-bottom">
                <div>
                    <h2>Editar Blog Comment {{ $blogComment->id }} - {{ $blogComment->author }}</h2>
                </div>
                <div class="no-margin-left">
                    <a class="btn btn-primary" href="{{ route('blog-comment') }}">
                      Volver
                    </a>
                </div>
            </div>
        </div>
        <div class="row card">
            <div class="col-xs-12 col-md-12">
                @if(@session('message'))
                    <div class="alert alert-success">
                        {{ @session('message') }}
                    </div>
                @endif
                @if(@session('error'))
                    <div class="alert alert-danger">
                        {{ @session('error') }}
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> Hubo algunos problemas con tu entrada.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::model($blogComment, ['method' => 'PUT','route' => ['blog-comment-edit', $blogComment->id]]) !!}
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <label>Nombre</label>
                            {!! Form::textarea('message', null, array('placeholder' => 'Mensaje','class' => 'form-control', 'id' => 'message')) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <label for="author">User</label>
                            <select name="author" id="author" class="form-control">
                                @if($anonym == "true")
                                    <option value="Anonimo">Anonimo</option>
                                @endif
                                @foreach($users as $user)
                                    <option value="{{ $user->email }}" {{ $user->email == $blogComment->author ? 'selected' : '' }}>{{ $user->id }} - {{ $user->email }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <label for="blogpost">Post</label>
                            <select name="blogpost" id="blogpost" class="form-control">
                                @foreach($posts as $post)
                                    <option value="{{ $post->id }}" {{ $post->id == $blogComment->post->id ? 'selected' : '' }}>{{ $post->id }} - {{ $post->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 text-left">
                        <button type="submit" class="btn btn-success">
                            Actualizar
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('styles')
@endsection

@section('scripts')
@endsection