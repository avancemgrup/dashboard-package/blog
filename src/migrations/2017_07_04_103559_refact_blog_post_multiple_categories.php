<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RefactBlogPostMultipleCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blog_posts', function (Blueprint $table) {
            $table->dropForeign('blog_posts_category_id_foreign');
            $table->dropColumn('category_id');
        });
        Schema::create('blog_category_blog_post', function (Blueprint $table) {
            $table->integer('blog_category_id')->unsigned();
            $table->foreign('blog_category_id')
                ->references('id')->on('blog_categories')
                ->onDelete('cascade');

            $table->integer('blog_post_id')->unsigned();
            $table->foreign('blog_post_id')
                ->references('id')->on('blog_posts')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_category_blog_post');
    }
}
