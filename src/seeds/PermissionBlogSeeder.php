<?php
namespace Avannubo\Blog\Seeds;
use Illuminate\Database\Seeder;
use App\Permission;
class PermissionBlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //BLOG ADMIN
        $person = new Permission([
            'name' => 'blog.admin',
            'display_name' => 'blog admin',
            'description' => 'Administrar Blog'
        ]);
        $person->save();

        //BLOG POSTS
        $person = new Permission([
            'name' => 'blog.posts.view',
            'display_name' => 'blog posts view',
            'description' => 'Ver posts'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'blog.posts.create',
            'display_name' => 'blog posts creat',
            'description' => 'Crear posts'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'blog.posts.edit',
            'display_name' => 'blog posts edit',
            'description' => 'Editar posts'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'blog.posts.delete',
            'display_name' => 'blog posts delete',
            'description' => 'Eliminar posts'
        ]);
        $person->save();

        //BLOG CATEGORIES
        $person = new Permission([
            'name' => 'blog.categories.view',
            'display_name' => 'blog categories view',
            'description' => 'Ver categorias'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'blog.categories.create',
            'display_name' => 'blog categories create',
            'description' => 'Crear categorias'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'blog.categories.edit',
            'display_name' => 'blog categories edit',
            'description' => 'Editar categorias'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'blog.categories.delete',
            'display_name' => 'blog categories delete',
            'description' => 'Eliminar categorias'
        ]);
        $person->save();

        
        // BLOG TAGS
        $person = new Permission([
            'name' => 'blog.tags.view',
            'display_name' => 'blog tags view',
            'description' => 'Ver etiquetas'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'blog.tags.create',
            'display_name' => 'blog tags create',
            'description' => 'Crear etiquetas'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'blog.tags.edit',
            'display_name' => 'blog tags edit',
            'description' => 'Editar etiquetas'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'blog.tags.delete',
            'display_name' => 'blog tags delete',
            'description' => 'Eliminar etiquetas'
        ]);
        $person->save();

        //BLOG COMMENTS
        $person = new Permission([
            'name' => 'blog.comments.view',
            'display_name' => 'blog comments view',
            'description' => 'Ver comentarios'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'blog.comments.create',
            'display_name' => 'blog comments create',
            'description' => 'Crear comentarios'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'blog.comments.edit',
            'display_name' => 'blog comments edit',
            'description' => 'Editar comentarios'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'blog.comments.delete',
            'display_name' => 'blog comments delete',
            'description' => 'Eliminar comentarios'
        ]);
        $person->save();
    }
}
