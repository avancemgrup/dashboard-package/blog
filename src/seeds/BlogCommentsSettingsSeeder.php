<?php
namespace Avannubo\Blog\Seeds;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlogCommentsSettingsSeeder extends Seeder
{
    /**
     * @author: Daniel Lopez
     * @date: 11/07/2017
     * @description Insert default settings for comments
     *
     * @return void
     */
    public function run()
    {
        // check if table users is empty
        if(DB::table('blog_comments_settings')->get()->count() == 0){

            DB::table('blog_comments_settings')->insert([
                [
                    'key' => 'anonym',
                    'value' => 'false',
                ],
            ]);

        } else { echo " The table is not empty"; }
    }
}
