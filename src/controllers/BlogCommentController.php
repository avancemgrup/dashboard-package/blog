<?php

namespace Avannubo\Blog\Controllers;

use App\User;
use Avannubo\Blog\Models\BlogComment;
use Avannubo\Blog\Models\BlogCommentSettings;
use Avannubo\Blog\Models\BlogPost;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BlogCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
         $search = $request->input('search');
        $blogComments = BlogComment::where('author','LIKE', '%'.$search.'%')->paginate(15)->appends('search', $search);
        foreach ($blogComments as $blogComment) {
            $blogComment->post;
        }
        return view('blog::comment.index',['blogComments' => $blogComments]);
    }

    /**
     * @author: Daniel Lopez
     * @date: 11/07/2017
     * @param integer $id
     * @description Return view and comment for one post
     *
     * @return \Illuminate\Http\Response
     */
    public function getPostComments($id)
    {
        $blogComments = BlogPost::find($id)->comments()->paginate(1);
        foreach ($blogComments as $blogComment) {
            $blogComment->post;
        }
        return view('blog::comment.index',['blogComments' => $blogComments]);
    }


    /**
     * @author: Daniel Lopez
     * @date: 11/07/2017
     * @description Show settings of comments
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function settings()
    {
        $anonym = BlogCommentSettings::where('key','anonym')->get()->first();
        return view('blog::comment.settings',['anonym' => $anonym]);
    }


    /**
     * @author: Daniel Lopez
     * @date: 11/07/2017
     * @description Update settings of comments
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function upadateSettings(Request $request)
    {
        $this->validate($request, [
            'anonym' => 'required',
        ]);
        
        /**
         * Update anonym
         */
        $anonym = BlogCommentSettings::where('key','anonym')->get()->first();
        if ($request->get('anonym') == 'true') {
            $anonym->value = "true";
        } else {
            $anonym->value = "false";
        }
        if ($anonym->save()) {
            return redirect()->route('blog-comment-settings')->with(
                'message', 'Ajuste de comentario  se a actualizado correctamente'
            );
        }

        return redirect()->route('blog-comment-settings')->with(
            'error', 'Ajuste de comentario  no encontrado'
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $anonym = BlogCommentSettings::where('key','anonym')->get()->first();
        $posts = BlogPost::all();
        $users = User::all();
        return view('blog::comment.create', ['posts' => $posts, 'users' => $users, 'anonym' => $anonym->value]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'message' => 'required|Max:400',
            'author' => 'required|Max:255',
            'blogpost' => 'required',
        ]);

        $blogPost = BlogPost::find($request->get('blogpost'));
        if ($blogPost) {
            $blogComment = new BlogComment();
            $blogComment->message = $request->input('message');
            $blogComment->author = $request->input('author');
            $blogComment->post()->associate($blogPost);
            $blogComment->save();

            return redirect()->route("blog-comment")->with(
                'message', 'Comentario creado correctamente'
            );
        }

        return redirect()->route("blog-comment")->with(
            'error', 'Comentario no encontrado'
        );

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $anonym = BlogCommentSettings::where('key','anonym')->get()->first();
        $blogComment = BlogComment::find($id);
        if($blogComment) {
            $posts = BlogPost::all();
            $users = User::all();
            return view('blog::comment.edit', ['blogComment' => $blogComment,'posts' => $posts, 'users' => $users, 'anonym' => $anonym->value]);
        }
        return redirect()->route('blog-comment')->with(
            'error','Comentario no encontrado'
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'message' => 'required|Max:400',
            'author' => 'required|Max:255',
            'blogpost' => 'required',
        ]);
        $blogComment = BlogComment::find($id);
        if($blogComment) {
            $blogPost = BlogPost::find($request->get('blogpost'));
            if ($blogPost) {
                $blogComment->message = $request->input('message');
                $blogComment->author = $request->input('author');
                $blogComment->post()->associate($blogPost);
                $blogComment->update();

                return redirect()->route("blog-comment-edit", $id)->with(
                    'message', 'Comentario  actualizado correctamente'
                );
            }

            return redirect()->route("blog-comment-edit", $id)->with(
                'message', 'Comentario no encontrado'
            );
        }

        return redirect()->route("blog-comment-edit", $id)->with(
            'error', 'Comentario no encontrado'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blogComment = BlogComment::find($id);

        if ($blogComment) {
            $blogComment->delete();

            return redirect()->route("blog-comment")->with(
                'message', 'Comentario se a eliminado correctamente'
            );
        }

        return redirect()->route("blog-comment")->with(
            'error', 'Comentario no encontrado'
        );
    }

    /**
     * @autor Daniel Lopez
     * @date 12/07/2017
     * @param Request $request
     * @description Remove the multiple blog posts
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroys(Request $request)
    {
        if ($request->has('comments_id')) {
            foreach ($request->get('comments_id') as $id) {
                $blogComment = BlogComment::find($id);

                if (!$blogComment) {
                    $request->session()->flash('error', 'Blog Comments not found');
                    return response()->json([
                        'error', 'Comentario no encontrado'
                    ]);
                }

                if (!$blogComment->delete()) {
                    return redirect()->route("blog-comment")->with(
                        'error', 'Comentario no eliminado correctamente'
                    );
                }
            }
        } else {
            $request->session()->flash('message', 'No Blog Comments for delete');
            return response()->json([
                'error', 'Comentario no encontrado'
            ]);
        }

        $request->session()->flash('message', 'Blog Comments delete successfully');
        return response()->json([
            'error', 'Comentario no eliminado correctamente'
        ]);

    }
}