<?php

namespace Avannubo\Blog\Controllers;

use Avannubo\Blog\Models\BlogCategory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BlogCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $search = $request->input('search');
        $blogCategories = BlogCategory::where('name','LIKE', '%'.$search.'%')->paginate(15)->appends('search', $search);
        return view('blog::category.index',['blogCategories' => $blogCategories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blog::category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['slug'] = str_slug($request->get('slug'));
        $this->validate($request, [
            'name' => 'required|Max:255|unique:blog_categories,name',
            'description' => 'required|Max:400',
            'slug' => 'required|Max:255|unique:blog_categories,slug',
        ]);

        $blogCategory = new BlogCategory();
        $blogCategory->name = $request->input('name');
        $blogCategory->description = $request->input('description');
        $blogCategory->slug = $request->input('slug');
        $blogCategory->save();

        return redirect()->route("blog-category")->with(
            'message', 'Categoria creada correctamente'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blogCategory = BlogCategory::find($id);
        if($blogCategory) {
            return view('blog::category.edit', ['blogCategory' => $blogCategory]);
        }
        return redirect()->route('blog-category')->with(
            'error','Categoria no encontrada'
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request['slug'] = str_slug($request->get('slug'));
        $this->validate($request, [
            'name' => 'required|Max:255|unique:blog_categories,name,' . $id,
            'description' => 'required|Max:400',
            'slug' => 'required|Max:255|unique:blog_categories,slug,' . $id,
        ]);

        $blogCategory = BlogCategory::find($id);
        if ($blogCategory) {

            if (BlogCategory::where('name',$request->get('name'))->get()->count() > 0 && $blogCategory->name != $request->get('name')) {
                return redirect()->route("blog-category-edit", ['id' => $id])->with(
                    'error', 'Categoria nombre duplicado'
                );
            }

            $blogCategory->name = $request->get('name');
            $blogCategory->description = $request->get('description');
            $blogCategory->slug = $request->get('slug');

            if ($blogCategory->update()) {
                return redirect()->route("blog-category-edit", ['id' => $id])->with(
                    'message', 'Categoria actualizada correctamente'
                );
            }

        }

        return redirect()->route("blog-category-edit", ['id' => $id])->with(
            'error', 'Categoria no encontrada'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blogCategory = BlogCategory::find($id);

        if ($blogCategory) {
            $blogCategory->delete();

            return redirect()->route("blog-category")->with(
                'message', 'Categoria eliminada correctamente'
            );
        }

        return redirect()->route("blog-category")->with(
            'error', 'Categoria no encontrada'
        );
    }

    /**
     * @autor Daniel Lopez
     * @date 12/07/2017
     * @param Request $request
     * @description Remove the multiple blog posts
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroys(Request $request)
    {
        if ($request->has('categories_id')) {
            foreach ($request->get('categories_id') as $id) {
                $blogCategory = BlogCategory::find($id);

                if (!$blogCategory) {
                    $request->session()->flash('error', 'Blog Category no encontrado');
                    return response()->json([
                        'error', 'Categoria no encontrada'
                    ]);
                }

                $blogCategory->delete();
            }
        } else {
            $request->session()->flash('message', 'No hay categoria para elliminar');
            return response()->json([
                'message', 'Categoria eliminada correctamente'
            ]);
        }

        $request->session()->flash('message', 'Categoria eliminada correctamente');
        return response()->json([
            'message', 'Categoria eliminada correctamente'
        ]);
    }
}
