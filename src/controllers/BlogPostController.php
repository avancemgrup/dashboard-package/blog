<?php

namespace Avannubo\Blog\Controllers;

use Avannubo\Blog\Models\BlogCategory;
use Avannubo\Blog\Models\BlogPost;
use Avannubo\Blog\Models\BlogTag;
use App\Http\Controllers\Controller;
use Avannubo\Seo\Models\Seo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class BlogPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $search = $request->input('search');
        $blogPosts = BlogPost::where('title','LIKE', '%'.$search.'%')->orderBy('id', 'desc')->paginate(15)->appends('search', $search);

        foreach ($blogPosts as $blogPost) {
            $blogPost->categories;
        }
        return view('blog::post.index',['blogPosts' => $blogPosts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = BlogCategory::all();
        return view('blog::post.create', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['slug'] = str_slug($request->get('slug'));
        $timeImage = time();

        $this->validate($request, [
            'title' => 'required|unique:blog_posts,title',
            'image' => 'required|image',
            'description' => 'required|Max:400',
            'content' => 'required|Max:65000',
            'status' => 'required|Max:255',
            'publicationdate' => 'required|date_format:Y-m-d',
            'publicationtime' => 'required|date_format:H:i',
            'slug' => 'required|max:255|unique:blog_posts,slug',
            'categories' => 'nullable|array|exists:blog_posts,id',
            'tags' => 'array',
        ]);

        $blogPost = new BlogPost();

        $image = $request->file('image');
        $imageName = $timeImage . '.' . $image->getClientOriginalExtension();
        Storage::putFileAs('public/posts', new File($image), $imageName);
        $blogPost->image = $imageName;

        $blogPost->title = $request->input('title');
        $blogPost->description = $request->input('description');
        $blogPost->content = $request->input('content');
        $blogPost->status = $request->input('status') == "true" ? true : false;
        $blogPost->slug = $request->input('slug');
        $blogPost->author_id = Auth::id();

        $blogPost->publication_at = Carbon::createFromFormat('Y-m-d H:i', $request->get('publicationdate') . " " . $request->get('publicationtime'))->toDateTimeString();

        $blogPost->save();

        $blogPost->categories()->sync($request->input('categories'));

        $tags_id = [];
        if ($request->has('tags')) {
            $tags = array_filter($request->get('tags'), function($value) { return $value !== '' && $value !== null; });
            foreach ($tags as $tag) {
                $blogTag = BlogTag::where('name', $tag)->get()->first();
                if (!$blogTag) {
                    $blogTag = new BlogTag();
                    $blogTag->name = $tag;
                    $blogTag->save();
                }
                $tags_id[] = $blogTag->id;
            }
        }
        $blogPost->tags()->sync($tags_id);

        if(class_exists(Seo::class)){
            $seos = new Seo([
                'title' => $request->input('title'),
                'author' => "blog post web",
                'description' => $request->input('description'),
                'keyword' => "",
                'robots' => "",
                'canonical_url' => "",
                'language' => "Spanish",
                'generator' => "Avannubo",
                'route' => route('sg.blog.post', ['id' => $blogPost->id, 'slug' => $request->input('slug')]),
                'og_title' => $request->input('title'),
                'og_description' => $request->input('description'),
                'og_type' => "article",
                'og_url' => route('sg.blog.post', ['id' => $blogPost->id, 'slug' => $request->input('slug')]),
                'og_site_name' => config('app.name'),
                'tw_card' => "summary",
                'tw_url' => route('sg.blog.post', ['id' => $blogPost->id, 'slug' => $request->input('slug')]),
                'tw_title' => $request->input('title'),
                'tw_description' => $request->input('description'),
                'gl_page_type' => "article",
                'gl_name' => $request->input('title'),
                'gl_description' => $request->input('description'),
            ]);

            $img1 = $timeImage.'_1.'.$image->getClientOriginalExtension();
            $img2 = $timeImage.'_2.'.$image->getClientOriginalExtension();
            $img3 = $timeImage.'_3.'.$image->getClientOriginalExtension();

            Storage::putFileAs('public/seos', new File($image), $img1);
            Storage::putFileAs('public/seos', new File($image), $img2);
            Storage::putFileAs('public/seos', new File($image), $img3);

            $seos->og_image = $img1;
            $seos->tw_image = $img2;
            $seos->gl_image = $img3;

            $seos->save();
        }

        return redirect()->route("blog-post")->with(
            'message', 'Post creado correctamente'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blogPost = BlogPost::find($id);
        if($blogPost) {
            $categories = BlogCategory::all();
            $blogPost->tags;
            $categoriesInUser = $blogPost->categories()->get()->pluck('id')->toArray();
            return view('blog::post.edit', ['blogPost' => $blogPost, 'categories' => $categories, 'categoriesInUser' => $categoriesInUser]);
        }
        return redirect()->route('blog-post')->with(
            'error','Post no encontrado'
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request['slug'] = str_slug($request->get('slug'));
        $this->validate($request, [
            'title' => 'required|Max:255',
            'image' => 'image',
            'description' => 'required|Max:400',
            'content' => 'required|Max:65000',
            'status' => 'required',
            'publicationdate' => 'required|date_format:Y-m-d',
            'publicationtime' => 'required|date_format:H:i',
            'slug' => 'required|max:255|unique:blog_posts,slug,' . $id,
            'categories' => 'nullable|array|exists:blog_posts,id',
            'tags' => 'array',
        ]);
        $blogPost = BlogPost::find($id);
        if($blogPost) {

            if (BlogPost::where('title',$request->get('title'))->get()->count() > 0 && $blogPost->title != $request->get('title')) {
                return redirect()->route("blog-post-edit", ['id' => $id])->with(
                    'error', 'Post nombre duplicado '
                );
            }

            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $imageName = time() . '.' . $image->getClientOriginalExtension();
                Storage::putFileAs('public/posts', new File($image), $imageName);
                $blogPost->image = $imageName;
            }

            $blogPost->title = $request->input('title');
            $blogPost->description = $request->input('description');
            $blogPost->content = $request->input('content');
            $blogPost->status = $request->input('status') == "true" ? true : false;
            $blogPost->slug = $request->input('slug');

            $blogPost->publication_at = Carbon::createFromFormat('Y-m-d H:i', $request->get('publicationdate') . " " . $request->get('publicationtime'))->toDateTimeString();

            $blogPost->categories()->sync($request->input('categories'));

            $tags_id = [];
            if ($request->has('tags')) {
                $tags = array_filter($request->get('tags'), function($value) { return $value !== '' && $value !== null; });
                foreach ($tags as $tag) {
                    $blogTag = BlogTag::where('name', $tag)->get()->first();
                    if (!$blogTag) {
                        $blogTag = new BlogTag();
                        $blogTag->name = $tag;
                        $blogTag->save();
                    }
                    $tags_id[] = $blogTag->id;
                }
            }
            $blogPost->tags()->sync($tags_id);

            if ($blogPost->save()) {
                return redirect()->route("blog-post-edit", ['id' => $id])->with(
                    'message', 'Post actualizado correctamente'
                );
            }

        }

        return redirect()->route("blog-post-edit", ['id' => $id])->with(
            'error', 'Post no encontrado'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blogPost = BlogPost::find($id);

        if ($blogPost) {
            if($blogPost->delete()){
                if (Storage::disk('public')->exists('posts/' . $blogPost->image)) {
                    Storage::disk('public')->delete('posts/' . $blogPost->image);
                }
                return redirect()->route("blog-post")->with(
                    'message', 'Post eliminado correctamente'
                );
            }
        }
        return redirect()->route("blog-post")->with(
            'error', 'Post no encontrado'
        );
    }

    /**
     * @autor Daniel Lopez
     * @date 12/07/2017
     * @param Request $request
     * @description Remove the multiple blog posts
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroys(Request $request)
    {
        if ($request->has('posts_id')) {
            foreach ($request->get('posts_id') as $id) {
                $blogPost = BlogPost::find($id);

                if (!$blogPost) {
                    $request->session()->flash('error', 'Blog Post no encontrado');
                    return response()->json([
                        'error', 'Post no encontrado'
                    ]);
                }

                $blogPost->delete();
            }
        } else {
            $request->session()->flash('message', 'No Blog Posts for delete');
            return response()->json([
                'message', 'Post eliminado correctamente'
            ]);
        }

        $request->session()->flash('message', 'Post se a eliminado correctamente');
        return response()->json([
            'message', 'Post eliminado correctamente'
        ]);
    }
}