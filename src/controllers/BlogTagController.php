<?php

namespace Avannubo\Blog\Controllers;

use Avannubo\Blog\Models\BlogTag;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BlogTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $search = $request->input('search');
        $blogTags = BlogTag::where('name','LIKE', '%'.$search.'%')->paginate(15)->appends('search', $search);
        return view('blog::tag.index',['blogTags' => $blogTags]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blog::tag.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|Max:255|unique:blog_tags,name',
        ]);

        $blogTag = new BlogTag();
        $blogTag->name = $request->input('name');
        $blogTag->save();

        return redirect()->route("blog-tag")->with(
            'message', 'Etiqueta creada correctamente'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blogTag = BlogTag::find($id);
        if($blogTag) {
            return view('blog::tag.edit', ['blogTag' => $blogTag]);
        }
        return redirect()->route('blog-tag')->with(
            'error','Etiqueta no encontrada'
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|Max:255|unique:blog_tags,name,' . $id,
        ]);

        $blogTag = BlogTag::find($id);
        if($blogTag) {

            if (BlogTag::where('name',$request->get('name'))->get()->count() > 0 && $blogTag->name != $request->get('name')) {
                return redirect()->route("blog-tag-edit", ['id' => $id])->with(
                    'error', 'Etiqueta, nombre duplicado'
                );
            }

            $blogTag->name = $request->get('name');

            if ($blogTag->update()) {
                return redirect()->route("blog-tag-edit", ['id' => $id])->with(
                    'message', 'Etiqueta actualizada correctamente'
                );
            }

        }

        return redirect()->route("blog-tag-edit", ['id' => $id])->with(
            'error', 'Etiqueta no encontrada'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blogTag = BlogTag::find($id);

        if ($blogTag) {
            $blogTag->delete();

            return redirect()->route("blog-tag")->with(
                'message', 'Etiqueta eliminada correctamente'
            );
        }

        return redirect()->route("blog-tag")->with(
            'error', 'Etiqueta no encontrada'
        );
    }

    /**
     * @autor Daniel Lopez
     * @date 13/07/2017
     * @param Request $request
     * @description Remove the multiple blog tags
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroys(Request $request)
    {
        if ($request->has('tags_id')) {
            foreach ($request->get('tags_id') as $id) {
                $blogTag = BlogTag::find($id);

                if (!$blogTag) {
                    $request->session()->flash('error', 'Etiqueta no encontrada');
                    return response()->json([
                        'error', 'Etiqueta no encontrada'
                    ]);
                }

                $blogTag->delete();
            }
        } else {
            $request->session()->flash('message', 'No hay etiqueta de blog');
            return response()->json([
                'message', 'Etiqueta eliminada correctamente'
            ]);
        }

        $request->session()->flash('message', 'Etiqueta eliminada correctamente');
        return response()->json([
            'message', 'Etiqueta se eliminada correctamente'
        ]);
    }
    
}