<?php

namespace Avannubo\Blog\Models;

use Illuminate\Database\Eloquent\Model;

class BlogCommentSettings extends Model
{
    protected $table = 'blog_comments_settings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key','value'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
