<?php

namespace Avannubo\Blog\Models;

use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
    protected $table = 'blog_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','description','slug',
    ];

    public function posts() {
        return $this->belongsToMany(BlogPost::class);
    }
}
