<?php

namespace Avannubo\Blog\Models;

use Illuminate\Database\Eloquent\Model;

class BlogPost extends Model
{
    protected $table = 'blog_posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','image','description','content','status','slug','category_id','publication_at'
    ];

    public function categories() {
        return $this->belongsToMany(BlogCategory::class);
    }

    public function tags() {
        return $this->belongsToMany(BlogTag::class);
    }

    public function comments() {
        return $this->hasMany(BlogComment::class);
    }

}
