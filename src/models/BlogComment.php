<?php

namespace Avannubo\Blog\Models;

use Illuminate\Database\Eloquent\Model;

class BlogComment extends Model
{
    protected $table = 'blog_comments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'message','author'
    ];

    public function post() {
        return $this->belongsTo(BlogPost::class, 'blog_post_id');
    }
}
