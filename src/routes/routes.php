<?php

Route::group(['middleware' => ['web','auth'], 'prefix' => 'administration'], function () {
    // Blog Category
    Route::get('blog/category', [
        'as' => 'blog-category',
        'uses' => '\Avannubo\Blog\Controllers\BlogCategoryController@index',
        'middleware' => 'permission:blog.categories.view|blog.categories.create|blog.categories.edit|blog.categories.delete'
    ]);
    Route::get('blog/category/add', [
        'as' => 'blog-category-add',
        'uses' => '\Avannubo\Blog\Controllers\BlogCategoryController@create',
        'middleware' => 'permission:blog.categories.create'
    ]);
    Route::get('blog/category/edit/{id}', [
        'as' => 'blog-category-edit',
        'uses' => '\Avannubo\Blog\Controllers\BlogCategoryController@edit',
        'middleware' => 'permission:blog.categories.edit'
    ]);
    Route::post('blog/category/add', [
        'as' => 'blog-category-add',
        'uses' => '\Avannubo\Blog\Controllers\BlogCategoryController@store',
        'middleware' => 'permission:blog.categories.create'
    ]);
    Route::put('blog/category/edit/{id}', [
        'as' => 'blog-category-edit',
        'uses' => '\Avannubo\Blog\Controllers\BlogCategoryController@update',
        'middleware' => 'permission:blog.categories.edit'
    ]);
    Route::delete('blog/category/{id}', [
        'as' => 'blog-category-delete',
        'uses' => '\Avannubo\Blog\Controllers\BlogCategoryController@destroy',
        'middleware' => 'permission:blog.categories.delete'
    ]);
    Route::delete('blog/categories', [
        'as' => 'blog-categories-delete',
        'uses' => '\Avannubo\Blog\Controllers\BlogCategoryController@destroys',
        'middleware' => 'permission:blog.categories.delete'
    ]);

    // Blog Tag
    Route::get('blog/tag', [
        'as' => 'blog-tag',
        'uses' => '\Avannubo\Blog\Controllers\BlogTagController@index',
        'middleware' => 'permission:blog.tags.view|blog.tags.create|blog.tags.edit|blog.tags.delete'
    ]);
    Route::get('blog/tag/add', [
        'as' => 'blog-tag-add',
        'uses' => '\Avannubo\Blog\Controllers\BlogTagController@create',
        'middleware' => 'permission:blog.tags.create'
    ]);
    Route::get('blog/tag/edit/{id}', [
        'as' => 'blog-tag-edit',
        'uses' => '\Avannubo\Blog\Controllers\BlogTagController@edit',
        'middleware' => 'permission:blog.tags.edit'
    ]);
    Route::post('blog/tag/add', [
        'as' => 'blog-tag-add',
        'uses' => '\Avannubo\Blog\Controllers\BlogTagController@store',
        'middleware' => 'permission:blog.tags.create'
    ]);
    Route::put('blog/tag/edit/{id}', [
        'as' => 'blog-tag-edit',
        'uses' => '\Avannubo\Blog\Controllers\BlogTagController@update',
        'middleware' => 'permission:blog.tags.edit'
    ]);
    Route::delete('blog/tag/{id}', [
        'as' => 'blog-tag-delete',
        'uses' => '\Avannubo\Blog\Controllers\BlogTagController@destroy',
        'middleware' => 'permission:blog.tags.delete'
    ]);
    Route::delete('blog/tags', [
        'as' => 'blog-tags-delete',
        'uses' => '\Avannubo\Blog\Controllers\BlogTagController@destroys',
        'middleware' => 'permission:blog.tags.delete'
    ]);

    // Blog Post
    Route::get('blog/post', [
        'as' => 'blog-post',
        'uses' => '\Avannubo\Blog\Controllers\BlogPostController@index',
        'middleware' => 'permission:blog.posts.view|blog.posts.create|blog.posts.edit|blog.posts.delete'
    ]);
    Route::get('blog/post/add', [
        'as' => 'blog-post-add',
        'uses' => '\Avannubo\Blog\Controllers\BlogPostController@create',
        'middleware' => 'permission:blog.posts.create'
    ]);
    Route::get('blog/post/edit/{id}', [
        'as' => 'blog-post-edit',
        'uses' => '\Avannubo\Blog\Controllers\BlogPostController@edit',
        'middleware' => 'permission:blog.posts.edit'
    ]);
    Route::post('blog/post/add', [
        'as' => 'blog-post-add',
        'uses' => '\Avannubo\Blog\Controllers\BlogPostController@store',
        'middleware' => 'permission:blog.posts.create'
    ]);
    Route::put('blog/post/edit/{id}', [
        'as' => 'blog-post-edit',
        'uses' => '\Avannubo\Blog\Controllers\BlogPostController@update',
        'middleware' => 'permission:blog.posts.edit'
    ]);
    Route::delete('blog/post/{id}', [
        'as' => 'blog-post-delete',
        'uses' => '\Avannubo\Blog\Controllers\BlogPostController@destroy',
        'middleware' => 'permission:blog.posts.delete'
    ]);
    Route::delete('blog/posts', [
        'as' => 'blog-posts-delete',
        'uses' => '\Avannubo\Blog\Controllers\BlogPostController@destroys',
        'middleware' => 'permission:blog.posts.delete'
    ]);

    // Blog Comment
    Route::get('blog/post/{id}/comments', [
        'as' => 'blog-post-comment',
        'uses' => '\Avannubo\Blog\Controllers\BlogCommentController@getPostComments',
        'middleware' => 'permission:blog.comments.view|blog.comments.create|blog.comments.edit|blog.comments.delete'
    ]);
    Route::get('blog/comment', [
        'as' => 'blog-comment',
        'uses' => '\Avannubo\Blog\Controllers\BlogCommentController@index',
        'middleware' => 'permission:blog.comments.view|blog.comments.create|blog.comments.edit|blog.comments.delete'
    ]);
    Route::get('blog/comment/add', [
        'as' => 'blog-comment-add',
        'uses' => '\Avannubo\Blog\Controllers\BlogCommentController@create',
        'middleware' => 'permission:blog.comments.create'
    ]);
    Route::get('blog/comment/edit/{id}', [
        'as' => 'blog-comment-edit',
        'uses' => '\Avannubo\Blog\Controllers\BlogCommentController@edit',
        'middleware' => 'permission:blog.comments.edit'
    ]);
    Route::post('blog/comment/add', [
        'as' => 'blog-comment-add',
        'uses' => '\Avannubo\Blog\Controllers\BlogCommentController@store',
        'middleware' => 'permission:blog.comments.create'
    ]);
    Route::put('blog/comment/edit/{id}', [
        'as' => 'blog-comment-edit',
        'uses' => '\Avannubo\Blog\Controllers\BlogCommentController@update',
        'middleware' => 'permission:blog.comments.edit'
    ]);
    Route::delete('blog/comment/{id}', [
        'as' => 'blog-comment-delete',
        'uses' => '\Avannubo\Blog\Controllers\BlogCommentController@destroy',
        'middleware' => 'permission:blog.comments.delete'
    ]);
    Route::delete('blog/comments', [
        'as' => 'blog-comments-delete',
        'uses' => '\Avannubo\Blog\Controllers\BlogCommentController@destroys',
        'middleware' => 'permission:blog.comments.delete'
    ]);

    // Blog Comment Settings

    Route::get('blog/comment/settings', [
        'as' => 'blog-comment-settings',
        'uses' => '\Avannubo\Blog\Controllers\BlogCommentController@settings',
        'middleware' => 'permission:blog.admin'
    ]);
    Route::put('blog/comment/settings', [
        'as' => 'blog-comment-settings',
        'uses' => '\Avannubo\Blog\Controllers\BlogCommentController@upadateSettings',
        'middleware' => 'permission:blog.admin'
    ]);
});